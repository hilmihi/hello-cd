FROM node:12-alpine
WORKDIR /app
COPY dist /app/dist
COPY package.json package-lock.json /app/
RUN npm i --production
ENTRYPOINT [ "npm", "run" ]
CMD ["start"]