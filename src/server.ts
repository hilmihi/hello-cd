import * as express from 'express'
import * as cors from 'cors'
import { createServer } from 'http';

const PORT = process.env["HELLO_PORT"] || 3001;

const app = express();
app.set("port", PORT);
app.use(cors());

//routing
app.get("/hello", (req, res)=>{
    res.write("Hello World");
    res.end()
});
app.all("*", (req, res)=> res.status(404).send());

const server = createServer(app);

export const startServer = () => {
    return server.listen(PORT, () => {
        console.log("server listen on port ", PORT);
    })
} 